# Async SerDes protobuf STM32F4 project.
## Build
git clone --recurse-submodules https://git.sciprog.center/VMarkov/stm32_serDes_protobuf_project.git 
### Manual Build
    $ cmake -S . -B build 
    $ cmake --build build
### Auto Build
    $./bld 
The executables located in the bin directory
## Programming
OpenOCD + st-link2 usage.

"tasks.json" in .vscode folder describes programming scenarios.
